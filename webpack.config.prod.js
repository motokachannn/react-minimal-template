// Production Webpack Configuration

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: path.join(__dirname, './src/index.js'),
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'static/js/[name]-[contenthash].bundle.js',
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'static/css/[contenthash].css',
    }),
    new HtmlWebpackPlugin({
      scriptLoading: 'defer',
      inject: 'body',
      template: path.join(__dirname, './public/index.html'),
    }),
  ],
  module: {
    rules: [
      {
        test: /.js$/i,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /.(png|jpg|gif|woff|woff2|ttf|svg)$/i,
        loader: 'file-loader',
        options: {
          name: 'static/files/[contenthash].[ext]',
        },
      },
    ],
  },
  optimization: {
    minimize: true,
    splitChunks: {
      chunks: 'all',
    },
    minimizer: [
      new TerserPlugin({
        parallel: true,
        minify: TerserPlugin.uglifyJsMinify,
        terserOptions: {
          annotations: false,
        },
      }),
      new CssMinimizerPlugin(),
    ],
    moduleIds: 'deterministic',
  },
};
