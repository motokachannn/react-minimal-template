# React Minimal Project Template

The default tool to generate react projects (the `create-react-app`) is great and includes a lot of tools and sets up everything you need for development and testing. However, not all people uses those.

This template aims to create minimal react project which includes necessary for development like `webpack`, `babel` and more. All of them are pre-configured and you can configure them by yourself by editing `webpack.config.js`. Documentation for configuring it can be viewed [here](https://webpack.js.org/configuration/).

## Requirements

* Node.js (Pick the latest LTS or Stable build)
* `npm` (Node Package Manager, usually bundled with Node.js install)

## Getting Started

1. Clone or download the repository. For cloning you can use `git clone https://gitlab.com/motokachannn/react-minimal-template`
2. Run `npm install` to install required dependencies.

## Configuring

There are 2 webpack configuration files which are `webpack.config.js` for development and `webpack.config.prod.js` for production. Check the official [webpack's configuration documentation](https://webpack.js.org/configuration/) for more details on how to configure.

## Compilation

This have 2 types of build configuration. `dev` and `prod` (for production).

To build for dev (includes sourcemaps, assets are not minified):

```
npm run build:dev
```

For production build (no source maps, minfied css and js):

```
npm run build:prod
```

## Live Preview

Run `npm run start`.

## Acknowledgement

This template contains files from [`create-react-app`](https://github.com/facebook/create-react-app)
