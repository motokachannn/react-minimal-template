const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'development',
  entry: path.resolve(__dirname, './src/index.js'),
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'static/js/[name]-[contenthash].bundle.js',
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'static/css/[name].css',
    }),
    new HtmlWebpackPlugin({
      scriptLoading: 'defer',
      inject: 'body',
      template: path.resolve(__dirname, './public/index.html'),
    }),
  ],
  module: {
    rules: [
      {
        test: /.js$/i,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /.(png|jpg|gif|woff|woff2|ttf|svg)$/i,
        loader: 'file-loader',
        options: {
          name: 'static/files/[contenthash].[ext]',
        },
      },
    ],
  },
  devtool: 'source-map',
};
